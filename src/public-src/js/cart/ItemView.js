define([
    'backbone',
    'backbone.marionette',
    './template',
    './Service'
], function (Backbone, Marionette, template, CartService) {
    return Marionette.ItemView.extend({
        template: template,

        ui: {
            input: 'input'
        },

        events: {
            'click button[data-action=recalculate]': 'recalculate',
            'click button[data-action=order]': 'order'
        },

        initialize: function () {
            this.listenTo(this.model, 'change', this.render);
        },

        _updateCart: function () {
            this.ui.input.each(function () {
                CartService.add(parseInt(this.dataset.id), parseInt(this.value) || 0);
            });
        },

        recalculate: function () {
            this._updateCart();
            this.model.fetch({
                data: CartService._data
            })
        },

        order: function () {
            var email = window.prompt('Provide us with your email, please!');
            if (email) {
                var phone = window.prompt('Provide us with your phone number, please!');
                if (phone) {
                    this._updateCart();
                    this.model.save({
                        email: email,
                        phone: phone,
                        cart: CartService._data
                    }).done(function (data) {
                        CartService.removeAll();
                        Backbone.history.navigate('order/' + data.orderId, {trigger: true, replace: true});
                    });
                }
            }
        }
    });
});