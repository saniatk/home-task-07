define([
    'backbone',
    './Model'
], function (Backbone, Model) {
    return Backbone.Collection.extend({
        url: '/products',
        model: Model
    });
});